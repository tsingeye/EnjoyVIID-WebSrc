# EnjoyVIID-WebSrc

#### Description
视图库前端web代码

#### Software Architecture
Software architecture description

#### Installation

1.  Clone Project
    git clone https://gitee.com/tsingeye/EnjoyVIID-WebSrc.git
2.  Enter Project
    cd TsingeyeVIID-UI
3.  Installation dependency
    npm install
4.  Start service
    npm run dev

#### Instructions

暂无

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
